/*Analizador lexico
Everardo Ramirez Rosales
compilado con gcc 7.3.0 y  el estandar c++11 
g++ *.cpp -std=c++11 -Wall -pedantic -Wextra
octubre 2018
*/

#include <iostream>
#include <fstream>
#include "scanner.h"
using namespace std;

int main()
{
    string path;
    cout << "Escriba el nombre del archivo: ";
    getline(cin, path);
    Scanner scanner(path);
    Scanner::token token;
    do
    {
        token = scanner.next();
        if (token.tipo == Scanner::type::_id)
            cout << "Identificador " << token.valor << endl;
        if (token.tipo == Scanner::type::_reservada)
            cout << "Reservada "<< token.valor << endl;
        if (token.tipo == Scanner::type::_parizq)
            cout << "parentesis izquierdo" << endl;
        if (token.tipo == Scanner::type::_parder)
            cout << "parentesis derecho" << endl;
        if (token.tipo == Scanner::type::_corizq)
            cout << "corchete izquierdo" << endl;
        if (token.tipo == Scanner::type::_corder)
            cout << "corchete derecho" << endl;
        if (token.tipo == Scanner::type::_suma)
            cout << "suma" << endl;
        if (token.tipo == Scanner::type::_resta)
            cout << "resta" << endl;
        if (token.tipo == Scanner::type::_mult)
            cout << "multiplicacion" << endl;
        if (token.tipo == Scanner::type::_div)
            cout << "division" << endl;
        if (token.tipo == Scanner::type::_coma)
            cout << "coma" << endl;
        if (token.tipo == Scanner::type::_pycoma)
            cout << "punto y coma" << endl;
        if (token.tipo == Scanner::type::_dospun)
            cout << "dos puntos" << endl;
        if (token.tipo == Scanner::type::_hxnum)
            cout << "numero hexadecimal " << token.valor << endl;
        if (token.tipo == Scanner::type::_ocnum)
            cout << "numero octal " << token.valor << endl;
        if (token.tipo == Scanner::type::_renum)
            cout << "numero real " << token.valor << endl;
    } while (token.tipo != Scanner::type::_eof && token.tipo != Scanner::type::_err);

    if (token.tipo == Scanner::type::_eof)
    {
        cout << "Fin de archivo lineas: " << scanner.lines << endl;
    }
    else
    {
        cout << "Error en la linea " << scanner.lines << endl;
    }
    cout << "Fin del computo" << endl;
    return 0;
}
