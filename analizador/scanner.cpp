/*
Everardo Ramirez Rosales
octubre 2018
*/

#include "scanner.h"
#include <cctype>

//funciones publicas
Scanner::Scanner(string filename)
{
    file.open(filename);
    q = file.tellg();
}

Scanner::~Scanner()
{
    file.close();
}

Scanner::token Scanner::next()
{
    wsp();
    if (comentario())
        return next(); //devuelve el proximo token

    token result = num();
    if (result.tipo != _err)
        return result;

    result = idYReservadas();
    if (result.tipo != _err)
        return result;

    result = monoCaracter();
    if (result.tipo != _err)
        return result;

    result = eof();
    if (result.tipo == _eof)
        return result;

    token isError;
    return isError;
}

//interfaz archivo scanner
char Scanner::read() { return file.get(); }
void Scanner::failure() { file.seekg(q); }
void Scanner::success() { q = file.tellg(); }
void Scanner::fallback() { file.seekg(-1, file.cur); }

//automatas

void Scanner::wsp()
{
    char c = read();
    while (isspace(c))
    {
        if (c == '\n')
            lines++;
        c = read();
    }
    fallback();
    success();
}

Scanner::token Scanner::num()
{
    string cadena = "";
    int actual = 1, prior = udef;
    while (actual != udef)
    {
        prior = actual;
        char c = read();
        cadena += c;
        switch (actual)
        {
        case 1:
            if (c == '0')
                actual = 2;
            else if (isdigit(c) && c !='0')
                actual = 3;
            else
                actual = udef;
            break;
        case 2:
            if (isdigit(c) && c !='8' && c !='9')
                actual = 4;
            else if (c == 'x' || c == 'X')
                actual = 5;
            else if (c == '.')
                actual = 6;
            else if (c == '8' || c == '9' || isalpha(c) || c == '_')
                actual = 13;
            else
                actual = udef;
            break;
        case 3:
            if (isdigit(c))
                actual = 3;
            else if (c == '.')
                actual = 6;
            else if (c == '_' || isalpha(c))
                actual = 13;
            else
                actual = udef;
            break;
        case 4:
            if (isdigit(c) && c !='8' && c !='9')
                actual = 4;
            else if (c == '8' || c == '9' || c == '_' || isalpha(c))
                actual = 13;
            else
                actual = udef;
            break;
        case 5:
            if (isxdigit(c))
                actual = 7;
            else
                actual = udef;
            break;
        case 6:
            if (isdigit(c))
                actual = 8;
            else
                actual = udef;
            break;
        case 7:
            if (isxdigit(c))
                actual = 9;
            else
                actual = udef;
            break;
        case 8:
            if (isdigit(c))
                actual = 8;
            else if (tolower(c) == 'e')
                actual = 10;
            else if (c == '_' || (isalpha(c) && tolower(c) != 'e'))
                actual = 13;
            else
                actual = udef;
            break;
        case 9:
            if (isxdigit(c))
                actual = 7;
            else if (c == '_' || (isalpha(c) && !isxdigit(c)))
                actual = 13;
            else
                actual = udef;
            break;
        case 10:
            if (isdigit(c))
                actual = 12;
            else if (c == '+' || c == '-')
                actual = 11;
            else
                actual = udef;
            break;
        case 11:
            if (isdigit(c))
                actual = 12;
            else
                actual = udef;
            break;
        case 12:
            if (isdigit(c))
                actual = 12;
            else if (c == '_' || isalpha(c))
                actual = 13;
            else
                actual = udef;
            break;
        case 13:
            actual = udef;
            break;
        }
    }

    if (prior == 2 || prior == 4)
    {
        fallback();
        success();
        return token(_ocnum, cadena.substr(0, cadena.length() - 1));
    }
    if (prior == 9)
    {
        fallback();
        success();
        return token(_hxnum, cadena.substr(0, cadena.length() - 1));
    }
    if (prior == 3 || prior == 8 || prior == 12)
    {
        fallback();
        success();
        return token(_renum, cadena.substr(0, cadena.length() - 1));
    }

    failure();
    return token();
}

Scanner::token Scanner::eof()
{
    read();
    if (file.tellg() == -1) //tellg cuando esta en e fin de archivo retorna -1 lo uso por que eof falla
    {
        success();
        return token(_eof);
    }
    failure();
    return token();
}

Scanner::token Scanner::idYReservadas()
{
    int actual = 0, prior = udef;
    string palabra = "";
    while (actual != udef)
    {
        prior = actual;
        char c = read();
        palabra += c;

        switch (actual)
        {
        case 0:
            if (isalpha(c))
                actual = 1;
            else if (c == '_')
                actual = 3;
            else
                actual = udef;
            break;
        case 1:
            if (isalnum(c) || c == '_')
                actual = 1;
            else if (c == '\'')
                actual = 2;
            else
                actual = udef;
            break;
        case 2:
            if (c == '\'')
                actual = 2;
            else if(isalnum(c) || c == '_')
                actual = 5;
            else
                actual = udef;
            break;
        case 3:
            if (c == '_')
                actual = 3;
            else if (isalpha(c))
                actual = 1;
            else if (isdigit(c))
                actual = 4;
            else
                actual = udef;
            break;
        case 4:
            if (isdigit(c))
                actual = 4;
            else if (isalpha(c))
                actual = 1;
            else if (c == '_')
                actual = 3;
            else
                actual = udef;
            break;
        case 5:
            actual = udef;
            break;
        }
    }

    for (int i = 0; i < 3; i++)
    {
        if (reservadas[i].compare(palabra.substr(0, palabra.length() - 1)) == 0)
        {
            fallback();
            success();
            return token(_reservada, reservadas[i]);
        }
    }

    if (prior == 1 || prior == 2)
    {
        fallback();
        success();
        return token(_id, palabra.substr(0, palabra.length() - 1));
    }

    failure();
    return token();
}

Scanner::token Scanner::monoCaracter()
{
    char c = read();
    type t;
    switch (c)
    {
    case '(':
        t = _parizq;
        break;
    case ')':
        t = _parder;
        break;
    case '[':
        t = _corizq;
        break;
    case ']':
        t = _corder;
        break;
    case '+':
        t = _suma;
        break;
    case '-':
        t = _resta;
        break;
    case '*':
        t = _mult;
        break;
    case '/':
        t = _div;
        break;
    case ',':
        t = _coma;
        break;
    case ';':
        t = _pycoma;
        break;
    case ':':
        t = _dospun;
        break;
    default:
        failure();
        return token();
    }
    success();
    return token(t);
}

bool Scanner::comentario()
{
    char c = read();
    if (c == '!')
    {
        string s;
        getline(file, s);
        lines++;
        success();
        return true;
    }
    failure();
    return false;
}