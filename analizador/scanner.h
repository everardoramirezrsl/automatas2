/*
Everardo Ramirez Rosales
octubre 2018
*/

#ifndef SCANNER_HPP
#define SCANNER_HPP
#include <fstream>
#include <string>

using namespace std;

class Scanner
{
public:
  int lines = 1;
  typedef enum
  {
    _id,
    _reservada,
    _renum,
    _hxnum,
    _ocnum,
    _eof,
    _err,
    _parizq,
    _parder,
    _corizq,
    _corder,
    _suma,
    _resta,
    _mult,
    _div,
    _coma,
    _pycoma,
    _dospun,
  } type;

  struct token
  {
    type tipo;
    string valor;
    token() : tipo(_err), valor(""){}
    token(type tipo, string valor) : tipo(tipo), valor(valor){}
    token(type tipo) : tipo(tipo), valor(""){}
  };

  token next();
  Scanner(string filename);
  ~Scanner();

private:
  const static int udef = -1;
  long q;

  ifstream file;
  const string reservadas[3] = {"identity", "transpose", "throw"};

  token idYReservadas();
  void wsp();
  token num();
  token eof();
  token monoCaracter();
  bool comentario();
  char read();
  void failure();
  void success();
  void fallback();
};

#endif