#include "ejemplo.h"
 
void Persona::caminar() {
    cout << nombre << " esta caminando..." << endl;
}
 
void Persona::mostrarDatos() {
    cout << "Nombre: " << nombre << "\nEdad: " << edad << endl;
}
 
void Persona::hablar() {
    cout << nombre << " esta hablando..." << endl;
}
