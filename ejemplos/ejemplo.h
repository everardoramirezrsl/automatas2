#ifndef EJEMPLO1_H_INCLUDED
#define EJEMPLO1_H_INCLUDED
 
#include <iostream>
using namespace std;
class Persona {
public:
    Persona(int _edad, char* _nombre) : edad(_edad), nombre(_nombre) { }
    void caminar();
    void mostrarDatos();
    void hablar();
private:
    int edad;
    char* nombre;
};
 
#endif // EJEMPLO1_H_INCLUDED
 