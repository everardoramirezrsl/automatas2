#include <iostream>
#include "ejemplo.h"

using namespace std;

int main(int argc, char const *argv[])
{
    Persona Pedro(21, (char*)"Pedro");
 
    Pedro.mostrarDatos();
    Pedro.hablar();
    Pedro.caminar();
 
    cout << endl;
 
    cout<<"Hola mundo"<<endl;
    return 0;
}
